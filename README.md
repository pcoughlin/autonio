# <img src="src/assets/img/icn.png" width="160px" height="100px" align="center" alt="Autonio icon">     - an open-source decentralized cryptocurrency trading terminal

[Autonio](https://www.autonio.foundation) Trading Terminal is a decentralized application that opens access to algorithmic trading techniques on the market's top cryptocurrency exchanges. Build, sell, or buy intelligent trading algorithms with a user-friendly interface.

#
![Backtest](https://gitlab.com/autonio/autonio/uploads/2cae59439045e8f7a506a41b9408bb23/backtest.gif)
#
![Livetrade](https://gitlab.com/autonio/autonio/uploads/b47d97954204afe8ec4f8528e1678823/livetrade.gif)
#
![Algorithm Marketplace](https://gitlab.com/autonio/autonio/uploads/1df69b4d35288b5e405404575dccb72b/algo-mkt-place.gif)
#
![Buy Algo](https://gitlab.com/autonio/autonio/uploads/f31cf3af0943ab64a90cc3bbe1038e23/buy-algo.gif)


This project adheres to the Contributor Covenant [code of conduct](CODE_OF_CONDUCT.md).
By participating, you are expected to uphold this code. Please report unacceptable
behavior to [contact@autonio.foundation](mailto:contact@autonio.foundation).

Follow [Autonio Updates](https://t.me/autonio_updates) on Telegram and [@AI_Autonio](https://twitter.com/AI_Autonio) on Twitter for important
announcements and updates. Visit the [Autonio website](https://www.autonio.foundation).

## Using

You can [download the latest release](https://old.autonio.foundation/index-2.html#download) for your operating system or run, build it yourself (see below).

## Run, Build

You'll need [Node.js](https://nodejs.org) installed on your computer in order to run or build this project.

```bash
$ git clone https://gitlab.com/autonio/autonio.git
$ cd Autonio
$ npm install
```

Run the project in the default browser
```bash
$ npm run ng serve
```
Run the project in electron
```bash
$ npm run electron
```

Build the project for Windows, Mac and linux system
```bash
electron-packager . --overwrite --asar=true --platform=win32 --arch=ia32 --icon=src/assets/img/logo.ico --prune=true --out=release-builds

electron-packager . --overwrite --asar=true --platform=darwin --arch=x64 --icon=dist/assets/img/logo.icns --prune=true --out=release-builds

electron-packager . --overwrite --asar=true --platform=linux --arch=x64 --icon=dist/assets/img/logo.png --prune=true --out=release-builds

```
please follow [this link](https://www.christianengvall.se/electron-packager-tutorial/) for initial setup of electron packager to build this project.

## Licensing

Autonio is licensed under the [Apache License](LICENSE), Version 2.0.
