import { Component, Injectable, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { TranslateService } from '@ngx-translate/core';
// import { ipcRenderer } from 'electron';
// import {sma}
// import { DemoComponent}
// const fs = (<any>window).require("fs");
// const electron = (<any>window).require("electron");

import * as $ from 'jquery';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

@Injectable()
export class AppComponent implements OnInit {
  href: string = '';
  autonio_token;
  nio_amt;
  give_acces: boolean = false;
  constructor(private router: Router,private translate: TranslateService) {
      translate.setDefaultLang('en');
   }

  electron_er(msg) {
    alert(msg);
  }
//Verify users for granting access to use platform as per number of NIO's they hold.
  checkUser(uname, goto, current) {
    $.ajax({
      url: 'https://api.etherscan.io/api?module=account&action=tokenbalance&contractaddress=0x5554e04e76533e1d14c52f05beef6c9d329e1e30&address='+ uname+'&tag=latest&apikey=YourApiKeyToken',
      success: (data) => {
        let received_amnt = parseInt(data.result);
       if(received_amnt>=parseInt(this.nio_amt)){
         this.router.navigateByUrl('/' + goto);
       }
       else{
         this.router.navigateByUrl('/membership');

       }


   },
   error: (data) => {
       this.electron_er('Some Error, Seems like your Internet Connection Issue. Please try again later');
        this.router.navigateByUrl('/' + current);
      }
    });
  }
  checkAccess(uname){
    $.ajax({
      method: 'POST',
      url: 'https://autonio.foundation/metamask_login/get_access_permision.php',
      data: { 'uname': uname },
      error: (data) => {
        this.electron_er('Some Error, Seems like your Internet Connection Issue. Please try again later');
      },
      success: (data) => {
        this.nio_amt=data;
        this.checkUser1(uname);
      }
    });

  }
  checkUpdate(uname){
    let current=0;
    let uname1=uname;
    let value=0;
    $.ajax({
      method: 'POST',
      url: 'https://autonio.foundation/metamask_login/get_update_info.php',
      data: { 'uname': uname1 },
      error: (data) => {
        // this.electron_er('Some Error, Seems like your Internet Connection Issue. Please try again later');
      },
      success: (data) => {
        value=parseInt(data);
        if(value>current){
          if (window.confirm('A new version update is available, press OK to download the update.'))
            {
              window.open('https://autonio.foundation/autonio_new/index.php#download','_blank');
            }

        }
        else if( value==current){
          // alert("You are using updated version. ");
        }
      }
    });

  }
  checkUser1(uname) {
    $.ajax({
      url: 'https://api.etherscan.io/api?module=account&action=tokenbalance&contractaddress=0x5554e04e76533e1d14c52f05beef6c9d329e1e30&address='+ uname+'&tag=latest&apikey=YourApiKeyToken',
      method: 'POST',
      data: { 'username': uname },
      success: (data) => {
        let received_amnt = parseInt(data.result);
       if(received_amnt>=parseInt(this.nio_amt)){
         this.give_acces=true;
         this.checkUpdate(uname);
         this.router.navigateByUrl('/');

       }
       else{
         this.router.navigateByUrl('/membership');

       }

   },
   error: (data) => {
       this.electron_er('Some Error, Seems like your Internet Connection Issue. Please try again later');
        this.router.navigateByUrl('/membership');
      }
    });
  }
  ngOnInit() {
      $('.sidebar .links a').click(function () {
        $('.sidebar .links a').removeClass('active');
        $(this).addClass('active');
      });
    if (localStorage.autonio_login_token != null) {
      this.autonio_token = JSON.parse(localStorage.getItem('autonio_login_token'));
      let uname = JSON.parse(localStorage.autonio_login_token).username;
      this.checkAccess(uname);
    }
    else {
      this.autonio_token = null;
      this.router.navigateByUrl('/login');
    }
    // this.checkUser();


    setInterval(() => {
      this.IsOnline();
    }, 3000);
  }
  switch(lang){
    this.translate.use(lang);
  }
  onClick(event) {
    var locDate=JSON.parse(localStorage.tab_live_trade).tab_live_trade;
    var target = event.currentTarget;
    var idAttr = target.id;
    var current = $('.sidebar .links a.active').attr('id');
    if( current=="live" && locDate==1 && idAttr!= "live"){
        if (window.confirm("Changes will not be save if you moved to others.")) {
           var tab_live_trade = '{ "tab_live_trade": 0 }';
           localStorage.setItem('tab_live_trade', tab_live_trade);
           this.router.navigateByUrl('/');
           $('.sidebar .links a').click(function () {
             $('.sidebar .links a').removeClass('active');
             $(this).addClass('active');
           });
        } else {
          if(JSON.parse(localStorage.tab_live_trade).tab_live_trade==1 ){
          $('.sidebar .links a').click(function () {
            $('.sidebar .links a').removeClass('active');
              $('.sidebar .links #live').addClass('active');
          });
        }
          return false;
        }
       }
     else{
       $('.sidebar .links a').click(function () {
         $('.sidebar .links a').removeClass('active');
         $(this).addClass('active');
       });
     }
     if(current=="backtest" && locDate==1 && idAttr!= "backtest" ){
       if (window.confirm("Save changes before you exit.") ) {
                var tab_live_trade = '{ "tab_live_trade": 0 }';
                localStorage.setItem('tab_live_trade', tab_live_trade);
                var dataBacktestForm = $('form').serializeArray();
                localStorage.setItem('tab_backtest_data', JSON.stringify(dataBacktestForm));
                this.router.navigateByUrl('/');
                $('.sidebar .links a').click(function () {
                  $('.sidebar .links a').removeClass('active');
                  $(this).addClass('active');
                });
        } else {
            var tab_live_trade = '{ "tab_live_trade": 0 }';
            localStorage.setItem('tab_live_trade', tab_live_trade);
            var dataBacktestForm = '{ "tab_backtest_data": 0 }';
            localStorage.setItem('tab_backtest_data', dataBacktestForm);
            this.router.navigateByUrl('/');
            $('.sidebar .links a').click(function () {
              $('.sidebar .links a').removeClass('active');
              $(this).addClass('active');
            });
        }
     }

    if (idAttr != 'backtest') {
      if (localStorage.autonio_login_token == null) {
        this.router.navigateByUrl('/login');
      }
      else {
        if(this.give_acces==true){
          this.router.navigateByUrl('/' + idAttr);
        }
        else{
          this.router.navigateByUrl('/membership');
        }
      }
    }
    else {
      this.router.navigateByUrl('/');
    }

  }
  goTo(event){
   var target = event.currentTarget;
   var idAttr = target.id;
   console.log("hello clicked on==",idAttr);
  }

  baseUrl = 'https://www.test.com';
  isOnline = false;
  online = true;
  status = "online";

  onlineCheck() {
    let xhr = new XMLHttpRequest();
    return new Promise((resolve, reject) => {
      xhr.onload = () => {
        // Set online status
        this.isOnline = true;
        resolve(true);
      };
      xhr.onerror = () => {
        // Set online status
        this.isOnline = false;
        reject(false);
      };
      xhr.open('GET', this.baseUrl, true);
      xhr.send();
    });
  }

  IsOnline() {
    this.onlineCheck().then(() => {
      this.online = true;
      this.status = "online";
    }).catch(() => {
      this.online = false;
      this.status = "offline";
    });
  }
  // out1 = DemoComponent.ngOnInit();
}
