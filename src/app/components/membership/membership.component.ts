import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-membership',
  templateUrl: './membership.component.html',
  styleUrls: ['./membership.component.css']
})
export class MembershipComponent implements OnInit {
  autonio_token;
  nio_amnt: number ;
  nio_address = '';

  constructor(private router: Router) { }

  electron_er(msg) {
    alert(msg);
  }
  //trigger from nginit to check access status of platform
  checkAccess(uname){
    $.ajax({
      method: 'POST',
      url: 'https://autonio.foundation/metamask_login/get_access_permision.php',
    //  url: 'https://auton.io/webservices/user_check.php',
      data: { 'uname': uname },
      error: (data) => {
        this.electron_er('Some Error, Seems like your Internet Connection Issue. Please try again later');
      },
      success: (data) => {
        this.nio_amnt=parseInt(data);
        // console.log("permission from backend==",this.nio_amt,data);
      }
    });
  }
  ngOnInit() {

    if (localStorage.autonio_login_token != null) {
      this.autonio_token = JSON.parse(localStorage.getItem('autonio_login_token'));
      let uname = JSON.parse(localStorage.autonio_login_token).username;
      this.checkAccess(uname);
    }
    else {
      this.autonio_token = null;
      this.router.navigateByUrl('/login');
    }

  }
//trigger on click of i have button
  checkPayment(uname) {
    $.ajax({
      url: 'https://api.etherscan.io/api?module=account&action=tokenbalance&contractaddress=0x5554e04e76533e1d14c52f05beef6c9d329e1e30&address='+ uname+'&tag=latest&apikey=YourApiKeyToken',
      success: (data) => {
        let received_amnt = parseInt(data.result);
       // console.log("Nio token in your eth address is=",received_amnt);
       if(received_amnt>=this.nio_amnt){
         this.router.navigateByUrl('/live');
       }
      }
    });
  }
  logout() {
    localStorage.clear();
    //console.log(localStorage.getItem('autonio_login_token'));

    this.router.navigateByUrl('/login');
  }
}
