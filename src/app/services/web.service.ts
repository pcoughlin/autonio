import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import * as $ from 'jquery';

@Injectable()
export class WebService {

  constructor(public http: Http) {
  }

  getPairs(exchange) {
    let url = 'https://api.cryptowat.ch/markets/' + exchange;

    return this.http.get(url)
      .map(res => res.json());
  }

  getBaseAsset(pair) {
    let url = 'https://api.cryptowat.ch/pairs/' + pair;

    return this.http.get(url)
      .map(res => res.json());
  }

}
