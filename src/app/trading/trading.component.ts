import { Component, OnInit } from '@angular/core';
import { WebService } from '../services/web.service';
import * as ti1 from 'technicalindicators';
// import * as _ from 'lodash';
// import { sma, SMA } from '../../../../node_modules/technicalindicators';
// import { ad, adosc } from 'technical-indicator';
import * as $ from 'jquery';
import * as ccxt from 'ccxt';

@Component({
  selector: 'app-trading',
  templateUrl: './trading.component.html',
  styleUrls: ['./trading.component.css']
})
export class TradingComponent implements OnInit {

  exchanges: string[];
  pairs: string[];
  pair_detail: string[];
  asset = 'BTC';
  base = 'USD';

  constructor(private webservice: WebService) { }
  ngOnInit() {
    // this.pairs = ['--select one--'];
    let ti = ti1['window'];
    let out = ti.sma({ period: 5, values: [1, 2, 3, 4, 5, 6, 7, 8, 9], reversedInput: true });
  }

  PairChange(pair) {
    let out = this.webservice.getBaseAsset(pair).subscribe((out) => {
      let res = out.result;
      this.asset = res['base']['symbol'];
      this.base = res['quote']['symbol'];
    });
  }

  ExchangeChange(val) {
    let out = this.webservice.getPairs(val).subscribe((pairs) => {
      this.pairs = (pairs.result)
      this.PairChange(this.pairs[0]['pair']);
    });



    // (async () => {
    //   let val1 = new ccxt[val];
    //   this.pair_detail = await val1.load_markets();
    //   this.pairs = Object.keys(this.pair_detail);
    // })()
  }

}
