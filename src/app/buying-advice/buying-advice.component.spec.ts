import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyingAdviceComponent } from './buying-advice.component';

describe('BuyingAdviceComponent', () => {
  let component: BuyingAdviceComponent;
  let fixture: ComponentFixture<BuyingAdviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyingAdviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyingAdviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
